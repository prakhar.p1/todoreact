import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import Todo from './Todo';

function App() {

  const [todo,setTodo]=useState([]);
  const [input,setInput]=useState("");

  const handleInputChange=(event)=>{
    setInput(event.target.value);
  }
  const handleSubmit=()=>{
    if(input.trim().length==0){
      console.log("Please enter something");
      return;
    }
    const currentTodo={
      id:Math.random(),
      task:input,
      isCompleted:false

    };

    setTodo((todo)=>{
      return [...todo,currentTodo];
    })
    setInput("");
  }

  const handleDelete=(id)=>{
    const newTodo=todo.filter(todo=>{
      return todo.id!==id;
    })
    setTodo(newTodo);
  }
  const handleMarkCompleted=(id)=>{
    console.log(id);
    todo.forEach(t=>{
      if(t.id===id){
        t.isCompleted=true;
      }
    })
    setTodo([...todo]);
  }
  return (
    <div className="main_div">
      <div className='center_div'>
        <h1>My Todos</h1>
        <br/>
        <div className='add'>
        <input placeholder='Add Items' value={input} onChange={handleInputChange}></input>
        <button onClick={handleSubmit} className='add-button'>+</button>
        </div>
        <ol>

        {todo.length>0 && todo.map((item,index)=>{
          return <Todo todo={item} key={`${item.id}`} onDelete={handleDelete} onCompleted={handleMarkCompleted}/>
        })}
        </ol>
      </div>
    </div>
  );
}

export default App;