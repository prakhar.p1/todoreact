import React from "react";
function Todo({todo,onDelete,onCompleted}){
    console.log(todo);

    const textStyle = {
        textDecoration: todo.isCompleted ? 'line-through' : 'none'
    }

    return (
    <div className="todo_style">
        <li>
            <h3 style={textStyle}>{todo.task}</h3>
            <div>
            {!todo.isCompleted && <button onClick={()=>onCompleted(todo.id)}>✅</button>}
            <button onClick={()=>onDelete(todo.id)}>🗑️</button>
            </div>
        </li>
    </div >
    )
}
export default Todo;